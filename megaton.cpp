///////////////////////////////////////////////////////////////////////////////
///////        University of Hawaii, College of Engineering
/////// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///////
/////// @file meagton.cpp
/////// @version 1.0
///////
/////// @author Xiaokang Chen <xiaokang@hawaii.edu>
/////// @date 8 Feb 2022
/////////////////////////////////////////////////////////////////////////////////


#include "megaton.h"


double fromMegatonToJoule( double megaton ) {
     
      return megaton / MEGATON_IN_A_JOULE;

}

double fromJouleToMegaton( double joule ) {
     
      return joule * MEGATON_IN_A_JOULE;

}
