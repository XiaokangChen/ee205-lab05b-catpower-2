///////////////////////////////////////////////////////////////////////////////
///////        University of Hawaii, College of Engineering
/////// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///////
/////// @file gge.h
/////// @version 1.0
///////
/////// @author Xiaokang Chen <xiaokang@hawaii.edu>
/////// @date 8 Feb 2022
////////////////////////////////////////////////////////////////////////////////


const double GGE_IN_A_JOULE            = 1/1.213e8;
const char GASOLINE_GALLON_EQUIVALENT  = 'g';

extern double fromGGEToJoule( double gge );
extern double fromJouleToGGE( double joule );
