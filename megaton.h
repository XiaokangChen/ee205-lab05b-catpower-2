///////////////////////////////////////////////////////////////////////////////
///////        University of Hawaii, College of Engineering
/////// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///////
/////// @file megaton.h
/////// @version 1.0
///////
/////// @author Xiaokang Chen <xiaokang@hawaii.edu>
/////// @date 8 Feb 2022
/////////////////////////////////////////////////////////////////////////////////


const double MEGATON_IN_A_JOULE        = 1/4.184e15;
const char MEGATON                     = 'm';

extern double fromMegatonToJoule( double megaton );
extern double fromJouleToMegaton( double joule );
