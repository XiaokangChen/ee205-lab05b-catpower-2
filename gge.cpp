///////////////////////////////////////////////////////////////////////////////
/////        University of Hawaii, College of Engineering
///// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
/////
///// @file gge.cpp
///// @version 1.0
/////
///// @author Xiaokang Chen <xiaokang@hawaii.edu>
///// @date 8 Feb 2022
///////////////////////////////////////////////////////////////////////////////


#include "gge.h"

double fromGGEToJoule( double gge ) {
     
      return gge / GGE_IN_A_JOULE;

}

double fromJouleToGGE( double joule ) {
     
      return joule * GGE_IN_A_JOULE;

}
