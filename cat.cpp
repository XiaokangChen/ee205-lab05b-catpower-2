///////////////////////////////////////////////////////////////////////////////
///////        University of Hawaii, College of Engineering
/////// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///////
/////// @file cat.cpp
/////// @version 1.0
///////
/////// @author Xiaokang Chen <xiaokang@hawaii.edu>
/////// @date 8 Feb 2022
/////////////////////////////////////////////////////////////////////////////////


#include "cat.h"


double fromCatPowerToJoule( double catPower ) {
      
      if ( catPower == 0 ) {
       
         return 0; 

      }

      return catPower / CATPOWER_IN_A_JOULE;

}

double fromJouleToCatPower( double joule ) {
     
      return joule * CATPOWER_IN_A_JOULE;

}
