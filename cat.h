///////////////////////////////////////////////////////////////////////////////
///////        University of Hawaii, College of Engineering
/////// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///////
/////// @file cat.h
/////// @version 1.0
///////
/////// @author Xiaokang Chen <xiaokang@hawaii.edu>
/////// @date 8 Feb 2022
/////////////////////////////////////////////////////////////////////////////////


const double CATPOWER_IN_A_JOULE       = 0;
const char CATPOWER                    = 'c';

extern double fromCatPowerToJoule( double catPower );
extern double fromJouleToCatPower( double joule );
